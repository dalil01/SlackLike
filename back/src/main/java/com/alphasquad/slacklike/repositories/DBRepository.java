package com.alphasquad.slacklike.repositories;

import com.alphasquad.slacklike.core.annotations.DBColumn;
import com.alphasquad.slacklike.core.annotations.DBColumnManyToOne;
import com.alphasquad.slacklike.core.annotations.DBTable;
import com.alphasquad.slacklike.core.database.Database;
import com.alphasquad.slacklike.models.AbstractModel;
import com.google.gson.Gson;

import java.lang.reflect.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DBRepository<T extends AbstractModel> implements IRepository<T> {

    private final Connection DBConnexion = Database.get().getConnection();

    private final Gson gson;
    private final Class<T> type;

    public DBRepository(Class<T> type) {
        this.gson = new Gson();
        this.type = type;
    }

    @Override
    public T add(T model) {
        StringBuilder req = new StringBuilder("INSERT INTO " + model.getClass().getDeclaredAnnotation(DBTable.class).name());

        Field[] fields = model.getClass().getDeclaredFields();
        int fieldsSize = fields.length;

        StringBuilder reqSecondPart = new StringBuilder(" (");
        StringBuilder reqLastPart = new StringBuilder(") " + "VALUES" + " (");

        for (int i = 0; i < fieldsSize; i++) {
            DBColumn DBColumn = fields[i].getDeclaredAnnotation(DBColumn.class);
            DBColumnManyToOne DBColumnManyToOne = fields[i].getDeclaredAnnotation(DBColumnManyToOne.class);

            if (DBColumn != null || DBColumnManyToOne != null) {
                fields[i].setAccessible(true);

                if (DBColumn != null) {
                    reqSecondPart.append(DBColumn.name());
                }

                if (DBColumnManyToOne != null) {
                    reqSecondPart.append(DBColumnManyToOne.name());
                }

                reqLastPart.append("?");
                if (i + 1 < fieldsSize && (fields[i + 1].getDeclaredAnnotation(DBColumn.class) != null || fields[i + 1].getDeclaredAnnotation(DBColumnManyToOne.class) != null)) {
                    reqSecondPart.append(", ");
                    reqLastPart.append(", ");
                }
            }
        }

        req.append(reqSecondPart).append(reqLastPart).append(")");

        System.out.println("DBRepository : add() -> req => " + req);

        try {
            PreparedStatement ps = this.DBConnexion.prepareStatement(req.toString(), Statement.RETURN_GENERATED_KEYS);

            for (int i = 0; i < fieldsSize; i++) {
                DBColumn DBColumn = fields[i].getDeclaredAnnotation(DBColumn.class);
                DBColumnManyToOne DBColumnManyToOne = fields[i].getDeclaredAnnotation(DBColumnManyToOne.class);
                if (DBColumn != null || DBColumnManyToOne != null) {
                    Field field = fields[i];
                    field.setAccessible(true);

                    if (DBColumn != null) {
                        ps.setObject(i+1, (field.get(model) != null) ? field.get(model).toString() : null);
                        System.out.println("\t" + field.get(model));
                    }

                    if (DBColumnManyToOne != null) {
                        AbstractModel m = (AbstractModel) field.get(model);
                        ps.setObject(i+1, m.getId());
                        System.out.println("\t" + m.getId());
                    }
                }
            }

            ps.executeUpdate();

            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                model.setId(generatedKeys.getLong(1));
            }

            this.DBConnexion.commit();

            ps.close();
            generatedKeys.close();
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }

        return model;
    }

    @Override
    public T find(Long id) {
        T model = null;

        StringBuilder req =  new StringBuilder("SELECT * FROM ").append(this.type.getDeclaredAnnotation(DBTable.class).name()).append(" WHERE id = '").append(id).append("'");
        System.out.println(req);

        try {
            Statement stmt = this.DBConnexion.createStatement();
            ResultSet result = stmt.executeQuery(req.toString());

            if (result.next()) {
                model = this.gson.fromJson(this.resultSetToJSON(result), this.type);
                System.out.println("Model : " + model);
            }

            stmt.close();
            result.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return model;
    }

    @Override
    public List<T> findBy(HashMap<String, String> condition) {
        if (condition.size() == 0) {
            return null;
        }

        List<T> modelList = new ArrayList<>();

        StringBuilder req = new StringBuilder("SELECT * FROM ").append(this.type.getDeclaredAnnotation(DBTable.class).name());

        int i = 0;
        for (String key : condition.keySet()) {
            if (i == 0) {
                req.append(" WHERE ");
                i++;
            } else {
                req.append(" AND ");
            }
            req.append(key).append(" = '").append(condition.get(key)).append("'");
        }
        req.append(";");

        System.out.println(req);

        try {
            Statement stmt = this.DBConnexion.createStatement();
            ResultSet result = stmt.executeQuery(req.toString());

            while (result.next()) {
                String modelJson = this.resultSetToJSON(result);

                Field[] fields = this.gson.fromJson(modelJson, this.type).getClass().getDeclaredFields();
                for (Field field : fields) {
                    DBColumnManyToOne DBColumnManyToOne = field.getDeclaredAnnotation(DBColumnManyToOne.class);
                    if (DBColumnManyToOne != null) {
                        field.setAccessible(true);

                        StringBuilder reqManyToOne = new StringBuilder("SELECT * FROM ").append(DBColumnManyToOne.joinTableName()).append(" WHERE ").append(DBColumnManyToOne.joinColumnName()).append(" = ").append(result.getString(DBColumnManyToOne.name()));

                        Statement stmtManyToOne = this.DBConnexion.createStatement();
                        ResultSet resultOneToMany = stmtManyToOne.executeQuery(reqManyToOne.toString());
                        while (resultOneToMany.next()) {
                            modelJson = this.addJsonChildInJsonParent(modelJson, field.getName(), this.resultSetToJSON(resultOneToMany));
                        }

                        System.out.println("reqManyToOne " + reqManyToOne);
                        stmtManyToOne.close();
                        resultOneToMany.close();
                    }
                }

                modelList.add(this.gson.fromJson(modelJson, this.type));
            }

            stmt.close();
            result.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println(modelList);

        return modelList;
    }

    public List<T> findWithCustomRequest(String req) {
        System.out.println(req);

        List<T> modelList = new ArrayList<>();

        try {
            Statement stmt = this.DBConnexion.createStatement();
            ResultSet result = stmt.executeQuery(req);

            while (result.next()) {
                modelList.add(this.gson.fromJson(this.resultSetToJSON(result), this.type));
            }

            stmt.close();
            result.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return modelList;
    }

    @Override
    public List<T> findAll() {
        List<T> modelList = new ArrayList<>();

        StringBuilder req =  new StringBuilder("SELECT * FROM ").append(this.type.getDeclaredAnnotation(DBTable.class).name());
        System.out.println(req);

        try {
            Statement stmt = this.DBConnexion.createStatement();
            ResultSet result = stmt.executeQuery(req.toString());

            while (result.next()) {
                modelList.add(this.gson.fromJson(this.resultSetToJSON(result), this.type));
            }

            stmt.close();
            result.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println(modelList);

        return modelList;
    }

    public T update(T model) {
        // TODO : improve
        /*T modelInDB = this.find(model.getId();
        if (modelInDB == null) {
            System.out.println(model + " does not exist.");
            return null;
        }*/

        StringBuilder req = new StringBuilder("UPDATE ").append(model.getClass().getDeclaredAnnotation(DBTable.class).name()).append(" SET ");

        Field[] fields = model.getClass().getDeclaredFields();
        int fieldsSize = fields.length;

        for (int i = 0; i < fieldsSize; i++) {
            DBColumn DBColumn = fields[i].getDeclaredAnnotation(DBColumn.class);
            if (DBColumn != null && !DBColumn.name().equals("id")) {
                fields[i].setAccessible(true);
                req.append(fields[i].getName()).append(" = ?");
                if (i + 1 < fieldsSize && fields[i + 1].getDeclaredAnnotation(DBColumn.class) != null) {
                    req.append(", ");
                }
            }
        }
        req.append(" WHERE id = '").append(model.getId()).append("';");

        System.out.println(req);

        try {
            PreparedStatement ps = this.DBConnexion.prepareStatement(req.toString());

            for (int i = 0; i < fieldsSize; i++) {
                DBColumn DBColumn = fields[i].getDeclaredAnnotation(DBColumn.class);
                if (DBColumn != null  && !DBColumn.name().equals("id")) {
                    Field field = fields[i];
                    field.setAccessible(true);
                    ps.setObject(i, field.get(model));
                    System.out.println("\t" + field.get(model));
                }
            }

            ps.executeUpdate();

            this.DBConnexion.commit();

            ps.close();
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }

        return model;
    }

    public T delete(T model) {
        if (model == null || this.find(model.getId()) == null) {
            System.out.println(model + " does not exist.");
            return null;
        }

        try {
            Statement stmt = this.DBConnexion.createStatement();
            StringBuilder req = new StringBuilder("DELETE FROM ").append(model.getClass().getDeclaredAnnotation(DBTable.class).name()).append(" WHERE id = ").append(model.getId());
            System.out.println(req);
            stmt.executeUpdate(req.toString());
            this.DBConnexion.commit();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return model;
    }

    private String resultSetToJSON(ResultSet rs) {
        StringBuilder buf = new StringBuilder();

        buf.append("{");
        try {
            ResultSetMetaData metaData = rs.getMetaData();
            int nColumns = metaData.getColumnCount();
            for (int i = 1; i <= nColumns; ++i) {
                if (!metaData.getColumnName(i).equals("password")) {
                    buf.append(metaData.getColumnName(i)).append(": '").append(rs.getString(i).replaceAll("'","\\\\'").replaceAll("\"","\\\\\"")).append("'");
                }
                if (i < nColumns && !metaData.getColumnName(i+1).equals("password"))
                    buf.append(", ");
            }
        } catch (SQLException e) {
            buf.append(e.getMessage());
            e.printStackTrace();
        }
        buf.append("}");

        System.out.println("resultSetToJSON() => : " + buf);

        return buf.toString();
    }

    private String addJsonChildInJsonParent(String parentJson, String childJsonAttribute, String childJson) {
        String json = parentJson.substring(0, parentJson.length() - 1) + ", " + childJsonAttribute + ": " +  childJson + "}";
        System.out.println("addJsonChildInJsonParent() => " + json + "}");
        return json;
    }

}

