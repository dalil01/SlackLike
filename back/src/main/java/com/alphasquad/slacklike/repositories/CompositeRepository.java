package com.alphasquad.slacklike.repositories;

import com.alphasquad.slacklike.models.AbstractModel;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;

public class CompositeRepository<T extends AbstractModel> implements IRepository<T> {

    private final IRepository<T> memory;
    private final IRepository<T> db;

    public CompositeRepository(Class<T> type) {
        this.memory = new MemoryRepository<>(type);
        this.db = new DBRepository<>(type);
    }

    @Override
    public T add(T model) throws InvocationTargetException, IllegalAccessException {
        T modelFromDB = this.db.add(model);
        this.memory.add(modelFromDB);
        return modelFromDB;
    }

    @Override
    public T find(Long id) {
        T memoryModel = this.memory.find(id);
        return (memoryModel != null) ? memoryModel : this.db.find(id);
    }

    @Override
    public List<T> findBy(HashMap<String, String> condition) {
        // TODO : Memory
        return this.db.findBy(condition);
    }

    @Override
    public List<T> findAll() {
        List<T> memoryModelList = this.memory.findAll();
        return (memoryModelList != null && memoryModelList.size() > 0) ? memoryModelList : this.db.findAll();
    }

    @Override
    public List<T> findWithCustomRequest(String req) {
        // TODO : Memory
        return this.db.findWithCustomRequest(req);
    }

    @Override
    public T update(T model) {
        // TODO : Memory
        return this.db.update(model);
    }

    @Override
    public T delete(T model) {
        this.memory.delete(model);
        return this.db.delete(model);
    }

}
