package com.alphasquad.slacklike.repositories;

import com.alphasquad.slacklike.core.annotations.DBTable;
import com.alphasquad.slacklike.models.AbstractModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MemoryRepository<T extends AbstractModel> implements IRepository<T> {

    // <tableName, tableRows>
    private final Map<String, List<T>> data = new HashMap<>();
    private final Class<T> type;

    public MemoryRepository(Class<T> type) {
        this.type = type;
    }

    @Override
    public T add(T model) {
        String tableName = model.getClass().getDeclaredAnnotation(DBTable.class).name();
        if (!this.data.containsKey(tableName)) {
            this.data.put(tableName, new ArrayList<>());
        }
        this.data.get(tableName).add(model);
        return model;
    }

    @Override
    public T find(Long id) {
        String tableName = this.type.getDeclaredAnnotation(DBTable.class).name();
        if (!this.data.containsKey(tableName)) {
            return null;
        }
        return this.data.get(tableName).stream().filter(m -> m.getId().equals(id)).findAny().orElse(null);
    }

    @Override
    public List<T> findBy(HashMap<String, String> condition) {
        // TODO
        return null;
    }

    @Override
    public List<T> findAll() {
        return this.data.get(this.type.getDeclaredAnnotation(DBTable.class).name());
    }

    @Override
    public List<T> findWithCustomRequest(String req) {
        // TODO
        return null;
    }

    @Override
    public T update(T model) {
        // TODO
        return null;
    }

    @Override
    public T delete(T model) {
        List<T> tableRows = this.data.get(model.getClass().getDeclaredAnnotation(DBTable.class).name());
        if (tableRows != null) {
            this.data.get(model.getClass().getDeclaredAnnotation(DBTable.class).name()).remove(model);
        }
        return model;
    }

}
