package com.alphasquad.slacklike.models;

import com.alphasquad.slacklike.core.annotations.DBColumn;
import com.alphasquad.slacklike.core.annotations.DBColumnManyToOne;
import com.alphasquad.slacklike.core.annotations.DBTable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@DBTable(name = "workspace_channel_messages")
public class WorkspaceChannelMessage extends AbstractModel {

    @DBColumn(name = "id", options = "INT PRIMARY KEY NOT NULL AUTO_INCREMENT")
    private Long id;

    @DBColumn(name = "content", options = "TEXT")
    private String content;

    @DBColumn(name = "addedAt", options = "TEXT")
    private String addedAt;

    @DBColumnManyToOne(name = "workspace_channel_id", options = "INT", joinTableName = "workspace_channels", joinColumnName = "id")
    private WorkspaceChannel channel;

    @DBColumnManyToOne(name = "workspace_member_id", options = "INT", joinTableName = "workspace_members", joinColumnName = "id")
    private WorkspaceMember member;

}
