package com.alphasquad.slacklike.models;

public abstract class AbstractModel {

    public abstract Long getId();
    public abstract void setId(Long id);

}
