package com.alphasquad.slacklike.models;

import com.alphasquad.slacklike.core.annotations.DBColumn;
import com.alphasquad.slacklike.core.annotations.DBColumnOneToMany;
import com.alphasquad.slacklike.core.annotations.DBTable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@DBTable(name = "users")
public class User extends AbstractModel {

    @DBColumn(name = "id", options = "INT PRIMARY KEY NOT NULL AUTO_INCREMENT")
    private Long id;

    @DBColumn(name = "firstname", options = "VARCHAR(255)")
    private String firstname;

    @DBColumn(name = "lastname", options = "VARCHAR(255)")
    private String lastname;

    @DBColumn(name = "email", options = "VARCHAR(255)")
    private String email;

    @DBColumn(name = "password", options = "VARCHAR(255)")
    private String password;

    @DBColumnOneToMany(name = "workspaces", joinColumnName = "id")
    private List<Workspace> workspaces = new ArrayList<>();

    public User(String firstname, String lastname, String email, String password) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
    }

}
