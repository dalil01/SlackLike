package com.alphasquad.slacklike.controllers;

import com.alphasquad.slacklike.core.iomessage.IOMessageCommand;
import com.alphasquad.slacklike.core.iomessage.OMessage;
import com.alphasquad.slacklike.core.iomessage.OMessageStatus;
import com.alphasquad.slacklike.core.server.Session;
import com.alphasquad.slacklike.models.User;
import com.alphasquad.slacklike.repositories.CompositeRepository;
import com.alphasquad.slacklike.utils.UPasswordEncoder;

import java.lang.reflect.InvocationTargetException;

public class UserController {

    private final CompositeRepository<User> userRepository = new CompositeRepository<>(User.class);

    public void add(Session session, User user) throws InvocationTargetException, IllegalAccessException {
        user.setPassword(UPasswordEncoder.encode(user.getPassword()));
        User newUser = this.userRepository.add(user);

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.ADD_USER);
        oMessage.setStatus((newUser != null) ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);
        oMessage.setUser(newUser);

        session.sendOMessage(oMessage);
    }

}