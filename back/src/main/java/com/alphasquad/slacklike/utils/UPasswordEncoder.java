package com.alphasquad.slacklike.utils;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UPasswordEncoder {

    public static String encode(String password) {
        String passwordEncoded = "";

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            BigInteger bi = new BigInteger(1, md.digest(password.getBytes(StandardCharsets.UTF_8)));
            passwordEncoded = bi.toString(16);
        } catch (NoSuchAlgorithmException exception) {
            exception.printStackTrace();
        }

        return passwordEncoded;
    }

}
