package com.alphasquad.slacklike.core.database;

import com.alphasquad.slacklike.core.annotations.DBColumn;
import com.alphasquad.slacklike.core.annotations.DBColumnManyToOne;
import com.alphasquad.slacklike.core.annotations.DBTable;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.reflections.Reflections;

import java.io.*;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.Arrays;
import java.util.Set;

public class Database {

    private Connection connection;

    private Database() {}

    public void initConnection(String url, String username, String password) {
        try {
            this.connection = DriverManager.getConnection(url, username, password);
            System.out.println("DB Connection established !");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Database get() {
        return Database.Instance.INSTANCE;
    }

    public Connection getConnection() {
        return this.connection;
    }

    public void createOrUpdateTables() {
        try {
           Statement stmt = this.connection.createStatement();

           Reflections reflections = new Reflections("com.alphasquad.slacklike");
           Set<Class<?>> models = reflections.getTypesAnnotatedWith(DBTable.class);

           for (Class<?> model : models) {
               String tableName = model.getDeclaredAnnotation(DBTable.class).name();

               Field[] fields = model.getDeclaredFields();
               int fieldsSize = fields.length;

               StringBuilder createTableReq = new StringBuilder("CREATE TABLE IF NOT EXISTS ").append(tableName).append(" (");
               for (int i = 0; i < fieldsSize; i++) {
                   DBColumn DBColumn = fields[i].getDeclaredAnnotation(DBColumn.class);
                   if (DBColumn != null) {
                       createTableReq.append(DBColumn.name()).append(" ").append(DBColumn.options());
                   }

                   DBColumnManyToOne DBColumnManyToOne = fields[i].getDeclaredAnnotation(com.alphasquad.slacklike.core.annotations.DBColumnManyToOne.class);
                   if (DBColumnManyToOne != null) {
                       createTableReq.append(DBColumnManyToOne.name()).append(" ").append(DBColumnManyToOne.options());
                   }

                   if (i + 1 < fieldsSize && (fields[i + 1].getDeclaredAnnotation(DBColumn.class) != null || fields[i + 1].getDeclaredAnnotation(com.alphasquad.slacklike.core.annotations.DBColumnManyToOne.class) != null)) {
                       createTableReq.append(", ");
                   }
               }
               createTableReq.append(");");
               System.out.println(createTableReq);
               stmt.executeUpdate(createTableReq.toString());
               System.out.println("DB Table " + tableName + " created !");
           }

           for (Class<?> model : models) {
               String tableName = model.getDeclaredAnnotation(DBTable.class).name();

               Field[] fields = model.getDeclaredFields();
               boolean canBeExecute = false;

               for (Field field : fields) {
                   StringBuilder alterTableAddForeignKeyReq = new StringBuilder("ALTER TABLE ").append(tableName).append(" ADD FOREIGN KEY ");

                   DBColumnManyToOne DBColumnManyToOne = field.getDeclaredAnnotation(com.alphasquad.slacklike.core.annotations.DBColumnManyToOne.class);
                   if (DBColumnManyToOne != null) {
                       alterTableAddForeignKeyReq.append("(").append(DBColumnManyToOne.name()).append(")").append(" REFERENCES ").append(DBColumnManyToOne.joinTableName()).append("(").append(DBColumnManyToOne.joinColumnName()).append(");");
                       canBeExecute = true;
                   }

                   if (canBeExecute) {
                       System.out.println(alterTableAddForeignKeyReq);
                       System.out.println("DB Table " + tableName + " updated !");
                       stmt.executeUpdate(alterTableAddForeignKeyReq.toString());
                       canBeExecute = false;
                   }
               }
           }

            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void loadSQLFiles(InputStream[] inputStreams) {
        ScriptRunner sr = new ScriptRunner(this.connection);
        Arrays.stream(inputStreams).forEach(SQLFile -> {
            Reader reader = new BufferedReader(new InputStreamReader(SQLFile));
            sr.runScript(reader);
        });
        System.out.println("SQL Files loaded !");
    }

    private static class Instance {
        private static final Database INSTANCE = new Database();
    }

}
