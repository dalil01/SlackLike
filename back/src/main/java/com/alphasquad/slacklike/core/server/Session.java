package com.alphasquad.slacklike.core.server;

import com.alphasquad.slacklike.core.iomessage.OMessage;
import com.alphasquad.slacklike.utils.UIOMessage;
import com.alphasquad.slacklike.utils.UWebSocket;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.*;
import java.util.UUID;

@Getter
@Setter
@ToString
public class Session {

    private String id;

    private final OutputStream outputStream;

    public Session(OutputStream out) {
        this.id = UUID.randomUUID().toString();
        this.outputStream = out;
    }

    public void sendOMessage(OMessage oMessage) {
        try {
            this.outputStream.write(UWebSocket.encodeFrame(UIOMessage.encode(oMessage)));
            this.outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
