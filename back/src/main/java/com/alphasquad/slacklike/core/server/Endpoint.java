package com.alphasquad.slacklike.core.server;

import com.alphasquad.slacklike.controllers.SecurityController;
import com.alphasquad.slacklike.controllers.UserController;
import com.alphasquad.slacklike.controllers.WorkspaceController;
import com.alphasquad.slacklike.core.iomessage.IMessage;
import com.alphasquad.slacklike.models.User;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class Endpoint implements IEndpoint {

    private final UserController userController = new UserController();
    private final SecurityController securityController = new SecurityController();
    private final WorkspaceController workSpaceController = new WorkspaceController();

    public static Endpoint get() {
        return Endpoint.Instance.INSTANCE;
    }

    @Override
    public void onOpen(Session session) {
        System.out.println("Endpoint : onOpen() -> session => " + session);
    }

    @Override
    public void onMessage(Session session, IMessage iMessage) throws InvocationTargetException, IllegalAccessException, IOException {
        // Not connected
        switch (iMessage.getCommand()) {
            case LOGIN -> this.securityController.login(session, iMessage.getUser());
            case ADD_USER -> this.userController.add(session, iMessage.getUser());
            default -> {
                User userAuthorized = this.securityController.getUserIfIsAuthorized(iMessage.getUser(), iMessage.getSessionID(), session);
                System.out.println("Endpoint : onMessage() -> userAuthorized => " + userAuthorized);
                if (userAuthorized != null) {
                    // Connected
                    switch (iMessage.getCommand()) {
                        case CREATE_WORKSPACE -> this.workSpaceController.createWorkspace(session, iMessage.getWorkspace(), userAuthorized);
                        case UPDATE_WORKSPACE -> this.workSpaceController.updateWorkspace(iMessage.getWorkspace(), userAuthorized);
                        case GET_USER_WORKSPACES -> this.workSpaceController.getUserWorkSpaces(session, userAuthorized);
                        case CREATE_WORKSPACE_CHANNEL -> this.workSpaceController.addWorkspaceChannel(iMessage.getWorkspaceChannel(), userAuthorized);
                        case GET_WORKSPACE_CHANNELS -> this.workSpaceController.getWorkspaceChannels(iMessage.getWorkspace(), userAuthorized);
                        case LEAVE_WORKSPACE -> this.workSpaceController.leaveWorkspace(iMessage.getWorkspace(), userAuthorized);
                        case GET_WORKSPACE_CHANNEL_MESSAGES -> this.workSpaceController.getWorkspaceChannelMessages(session, iMessage.getWorkspaceChannel());
                        case ADD_WORKSPACE_CHANNEL_MESSAGE -> this.workSpaceController.addWorkspaceChannelMessage(iMessage.getWorkspace(), iMessage.getWorkspaceChannel(), iMessage.getWorkspaceChannelMessage(), userAuthorized);
                        case ADD_WORKSPACE_MEMBER -> this.workSpaceController.addWorkspaceMember(iMessage.getWorkspaceMember(), userAuthorized);
                        case DELETE_WORKSPACE_MEMBER -> this.workSpaceController.deleteWorkspaceMember(iMessage.getWorkspaceMember(), userAuthorized);
                        case LOGOUT -> this.securityController.logOut(iMessage.getUser().getId());
                    }
                }
            }
        }
    }

    @Override
    public void onClose(Session session) {
        System.out.println("Endpoint : onClose() -> session => " + session);
    }

    @Override
    public void onError(Session session, Throwable throwable) {
        System.out.println("Endpoint : onError() -> session => " + session);
        System.out.println("Endpoint : onError() -> throwable => " + throwable);
    }

    private static class Instance {
        private static final Endpoint INSTANCE = new Endpoint();
    }

}
