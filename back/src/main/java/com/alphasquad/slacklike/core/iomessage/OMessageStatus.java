package com.alphasquad.slacklike.core.iomessage;

public enum OMessageStatus {
    SUCCESS,
    ERROR
}
