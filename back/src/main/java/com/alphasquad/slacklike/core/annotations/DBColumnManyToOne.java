package com.alphasquad.slacklike.core.annotations;

import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DBColumnManyToOne {
    String name();
    String options() default "";
    String joinTableName();
    String joinColumnName();
}