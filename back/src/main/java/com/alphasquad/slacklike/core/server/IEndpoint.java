package com.alphasquad.slacklike.core.server;

import com.alphasquad.slacklike.core.iomessage.IMessage;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public interface IEndpoint {

    void onOpen(Session session);

    void onMessage(Session session, IMessage iMessage) throws InvocationTargetException, IllegalAccessException, IOException;

    void onClose(Session session);

    void onError(Session session, Throwable throwable);

}
