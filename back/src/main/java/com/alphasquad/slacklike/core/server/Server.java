package com.alphasquad.slacklike.core.server;

import com.alphasquad.slacklike.core.iomessage.OMessage;
import com.alphasquad.slacklike.utils.UWebSocket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public record Server(int port) {

    // <userID, session>
    public static Map<Long, Session> connectedUsers = new HashMap<>();

    // <workspaceID, Set<userID>>
    public static Map<Long, Set<Long>> openedWorkspaces = new HashMap<>();

    public void run() throws IOException {
        ExecutorService threadPool = Executors.newCachedThreadPool();

        try (ServerSocket serverSocket = new ServerSocket(this.port)) {
            System.out.println("Waiting a connection...");

            while (!serverSocket.isClosed()) {
                Socket socket = serverSocket.accept();
                System.out.println("A client is connected from " + socket.getInetAddress().getHostAddress());

                if (UWebSocket.handshake(socket.getInputStream(), socket.getOutputStream())) {
                    threadPool.execute(new SocketHandler(socket, Endpoint.get()));
                } else {
                    socket.close();
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } finally {
            threadPool.shutdown();
        }
    }

    public static void broadcastToWorkspaceMembers(Long workspaceID, Long userWhoSendMessageID, OMessage oMessage) {
        Set<Long> userIDs = Server.openedWorkspaces.get(workspaceID);
        userIDs.forEach(userID -> {
            Session session = Server.connectedUsers.get(userID);
            if (session != null) {
                oMessage.setUserWhoSendIMessageID(userWhoSendMessageID);
                session.sendOMessage(oMessage);
                System.out.println("Server : broadcastToWorkspaceMembers() -> WorkspaceMember(" + userID + ") broadcast...");
            } else {
                userIDs.remove(userID);
            }
        });
        System.out.println("openedWorkspaces -> " + Server.openedWorkspaces);
    }

}
