import { WorkspaceChannelType } from "../models/workspace-channel.type";
import {WorkspaceChannelMessageType} from "../models/workspace-channel-message.type";
import { UserType } from "../models/user.type";
import { WorkspaceType } from "../models/workspace.type";
import {IOMessageCommandEnum} from "../../enums/i-o-message-command.enum";
import {OMessageStatusEnum} from "../../enums/o-message-status.enum";
import { WorkspaceMemberType } from "../models/workspace-member.type";

export type IOMessageType = {
  userWhoSendIMessageID?: number,
  command: IOMessageCommandEnum,
  sessionID?: string | null | undefined,
  status?: OMessageStatusEnum
  user: UserType | null | undefined,
  workspace?: WorkspaceType | null,
  workspaceChannel?: WorkspaceChannelType | null,
  workspaceChannelMessage?: WorkspaceChannelMessageType | null,
  workspaceMember?: WorkspaceMemberType | null
}
