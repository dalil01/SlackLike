import { UserType } from "../models/user.type";

export type UserSessionType = {
  user: UserType,
  session: string | null | undefined
}
