import {WorkSpaceMemberRoleEnum} from "../../enums/workspace-member-role.enum";
import { UserType } from "./user.type";
import { WorkspaceType } from "./workspace.type";

export type WorkspaceMemberType = {
  id?: number,
  role?: WorkSpaceMemberRoleEnum,
  workspace: WorkspaceType,
  member: UserType
}
