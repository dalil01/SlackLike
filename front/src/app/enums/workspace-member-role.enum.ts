export enum WorkSpaceMemberRoleEnum {
  ADMIN = 'ADMIN',
  MEMBER = 'MEMBER'
}
