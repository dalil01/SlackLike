import { Component, OnDestroy, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import { Subscription } from 'rxjs';
import { SecurityService } from 'src/app/services/security/security.service';
import {WebsocketService} from "../../services/websocket/websocket.service";
import {IOMessageCommandEnum} from "../../enums/i-o-message-command.enum";
import {AlertType} from "../../types/alert/alert.type";
import {IOMessageType} from "../../types/iomessage/IOMessage.type";

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.css']
})
export class PortalComponent implements OnInit, OnDestroy {

  public signUpForm!: FormGroup;
  public loginForm!: FormGroup;

  public messageReceived!: IOMessageType;

  public alert!: AlertType;
  public signUpSuccessMessage : string = "Congrats, You've successfully signed up to SlackLike ! ";
  public errorMessage : string = "ERROR something went wrong try again.";

  private subscription!: Subscription | undefined;

  constructor(private formBuilder: FormBuilder, private webSocketService: WebsocketService, private securityService: SecurityService) { }

  ngOnInit(): void {
    this.initSignUpForm();
    this.initLoginForm();
  }

  private initSignUpForm(): void {
    this.signUpForm = this.formBuilder.group({
      firstname: new FormControl(''),
      lastname: new FormControl(''),
      email: new FormControl('', [Validators.email]),
      password: new FormControl('')
    });
  }

  private initLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [Validators.email]),
      password: new FormControl('')
    });
  }

  onSubmitSignUpForm(): void {
    this.webSocketService.reConnect();

    if (this.signUpForm.valid) {
      const IMessage: IOMessageType = {
        command: IOMessageCommandEnum.ADD_USER,
        sessionID: '',
        user: {
          firstname: this.signUpForm.value.firstname,
          lastname: this.signUpForm.value.lastname,
          email: this.signUpForm.value.email,
          password: this.signUpForm.value.password
        }
      };
      this.webSocketService.sendIMessage(IMessage);

      this.subscription = this.webSocketService.getWebSocket().subscribe(msg => {
          this.messageReceived = <IOMessageType> msg;
          console.log('message received : ' + JSON.stringify(this.messageReceived));
          this.alert = {
            type: this.messageReceived.status,
            message: (this.messageReceived.status == 'SUCCESS') ? this.signUpSuccessMessage : this.errorMessage
          }
        },
        err => {
          this.alert = {
            type: 'ERROR',
            message: this.errorMessage
          }
          console.log(err);
        }
      );
      this.signUpForm.reset();
    }
  }

  onSubmitLoginForm(): void {
    this.webSocketService.reConnect();

    if (this.loginForm.valid) {
      const IMessage : IOMessageType = {
        command: IOMessageCommandEnum.LOGIN,
        user: { email: this.loginForm.value.email, password: this.loginForm.value.password }
      };
      this.webSocketService.sendIMessage(IMessage);

      this.subscription = this.webSocketService.getWebSocket().subscribe(msg => {
          this.messageReceived = <IOMessageType> msg;
          console.log('message received : ' + JSON.stringify(this.messageReceived));

          if (msg.command == IOMessageCommandEnum.LOGIN) {
            this.alert = {
              type: this.messageReceived.status,
              message: (this.messageReceived.status == 'ERROR') ? "Invalid username or password." : null
            }
            if (this.messageReceived.status == 'SUCCESS')
              this.securityService.setUserSessionInLocalStorage({ user: { id: msg.user.id, firstname: msg.user.firstname, lastname: msg.user.lastname}, session: this.messageReceived.sessionID });
          }
        },
        err => {
          this.alert = {
            type: 'ERROR',
            message: this.errorMessage
          }
          console.log(err);
        }
      );
      this.loginForm.reset();
    }
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.webSocketService.reConnect();
  }

}
