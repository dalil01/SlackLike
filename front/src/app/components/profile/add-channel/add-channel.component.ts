import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {WebsocketService} from "../../../services/websocket/websocket.service";
import {IOMessageCommandEnum} from "../../../enums/i-o-message-command.enum";
import {IOMessageType} from "../../../types/iomessage/IOMessage.type";
import {SecurityService} from "../../../services/security/security.service";
import {WorkspaceType} from 'src/app/types/models/workspace.type';

@Component({
  selector: 'app-add-channel',
  templateUrl: './add-channel.component.html',
  styleUrls: ['./add-channel.component.css']
})
export class AddChannelComponent implements OnInit {

  @Input()
  currentWorkspace!: WorkspaceType;

  public channelForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private securityService: SecurityService, private webSocketService: WebsocketService) {
  }

  ngOnInit(): void {
    this.initChannelForm();
  }

  private initChannelForm(): void {
    this.channelForm = this.formBuilder.group({
      name: new FormControl('')
    });
  }

  onSubmitChannelForm(): void {
    if (this.channelForm.valid) {
      const IMessage: IOMessageType = {
        command: IOMessageCommandEnum.CREATE_WORKSPACE_CHANNEL,
        sessionID: this.securityService.getSessionFromLocalStorage().session,
        user: { id: this.securityService.getSessionFromLocalStorage().user.id },
        workspaceChannel: {
          workspace: this.currentWorkspace,
          name: this.channelForm.value.name
        }
      };
      this.webSocketService.sendIMessage(IMessage);
      this.channelForm.reset();
    }
  }

}
