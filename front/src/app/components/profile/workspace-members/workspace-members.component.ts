import {Component, Input, OnInit} from '@angular/core';
import {WorkspaceMemberType} from 'src/app/types/models/workspace-member.type';
import {IOMessageType} from "../../../types/iomessage/IOMessage.type";
import {IOMessageCommandEnum} from "../../../enums/i-o-message-command.enum";
import {SecurityService} from "../../../services/security/security.service";
import {WebsocketService} from "../../../services/websocket/websocket.service";
import {UserType} from "../../../types/models/user.type";

@Component({
  selector: 'app-workspace-members',
  templateUrl: './workspace-members.component.html',
  styleUrls: ['./workspace-members.component.css']
})
export class WorkspaceMembersComponent implements OnInit {

  @Input()
  public members: WorkspaceMemberType[] = [];

  @Input()
  public isConfig: boolean = false;

  @Input()
  public currentMember!: WorkspaceMemberType;

  public authUser!: UserType;

  constructor(private securityService: SecurityService, private webSocketService: WebsocketService) {
  }

  ngOnInit(): void {
    this.authUser = this.securityService.getSessionFromLocalStorage().user;
  }

  deleteWorkspaceMember(workspaceMember: WorkspaceMemberType): void {
    const iMessage: IOMessageType = {
      command: IOMessageCommandEnum.DELETE_WORKSPACE_MEMBER,
      sessionID: this.securityService.getSessionFromLocalStorage().session,
      user: this.securityService.getSessionFromLocalStorage().user,
      workspaceMember: { id: workspaceMember?.id,  member: { id: workspaceMember.member.id }, workspace: { id: workspaceMember.workspace.id } },
    };
    this.webSocketService.sendIMessage(iMessage);
  }

}
