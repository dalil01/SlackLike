import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {WorkspaceChannelType} from 'src/app/types/models/workspace-channel.type';
import {IOMessageType} from "../../../types/iomessage/IOMessage.type";
import {IOMessageCommandEnum} from "../../../enums/i-o-message-command.enum";
import {WebsocketService} from "../../../services/websocket/websocket.service";
import {SecurityService} from "../../../services/security/security.service";
import {WorkspaceType} from "../../../types/models/workspace.type";
import {UserType} from 'src/app/types/models/user.type';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css']
})
export class ChannelComponent implements OnInit {

  @Input()
  public currentWorkspace!: WorkspaceType;

  @Input()
  public currentChannel!: WorkspaceChannelType;

  public messageForm!: FormGroup;

  public authUser!: UserType;

  constructor(private formBuilder: FormBuilder, private securityService: SecurityService, private webSocketService: WebsocketService) {
  }

  private initMessageForm(): void {
    this.messageForm = this.formBuilder.group({
      content: new FormControl('')
    });
  }

  ngOnInit(): void {
    this.initMessageForm();
    this.authUser = this.securityService.getSessionFromLocalStorage().user;
  }

  onSubmitMessageForm(): void {
    if (this.messageForm.valid) {
      const IMessage: IOMessageType = {
        command: IOMessageCommandEnum.ADD_WORKSPACE_CHANNEL_MESSAGE,
        sessionID: this.securityService.getSessionFromLocalStorage().session,
        user: { id: this.securityService.getSessionFromLocalStorage().user.id },
        workspace: { id: this.currentWorkspace.id },
        workspaceChannel: { id: this.currentChannel.id, name: this.currentChannel.name, workspace: { id: this.currentChannel.workspace.id } },
        workspaceChannelMessage: {
          content: this.messageForm.value.content,
          channel: this.currentChannel
        }
      };
      this.webSocketService.sendIMessage(IMessage);
      this.messageForm.reset();
    }
  }

}
