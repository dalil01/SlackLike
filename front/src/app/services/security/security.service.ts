import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {UserSessionType} from "../../types/security/user-session.type";
import {IOMessageType} from "../../types/iomessage/IOMessage.type";
import {IOMessageCommandEnum} from "../../enums/i-o-message-command.enum";
import {WebsocketService} from "../websocket/websocket.service";

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  private userSessionKey: string = "user-session";
  private userSessionInLocalStorageSubject: Subject<string> = new Subject<string>();

  constructor(private webSocketService: WebsocketService) {
  }

  public getUserSessionInLocalStorageSubject(): Subject<string> {
    return this.userSessionInLocalStorageSubject;
  }

  public setUserSessionInLocalStorage(userSession: UserSessionType): void {
    if(userSession){
      localStorage.setItem(this.userSessionKey, JSON.stringify(userSession));
      this.userSessionInLocalStorageSubject.next(JSON.stringify(userSession));
    }
  }

  public removeSessionFromLocalStorage(): void {
    localStorage.removeItem(this.userSessionKey);
    this.userSessionInLocalStorageSubject.next("");
  }

  public getSessionFromLocalStorage(): UserSessionType {
    // @ts-ignore
    return JSON.parse(localStorage.getItem(this.userSessionKey)) || { user: '', session: '' };
  }

  public isAuth(): boolean {
    return localStorage.getItem(this.userSessionKey) != null;
  }

  public logOut(): void {
    const iMessageLogOut : IOMessageType = {
      command: IOMessageCommandEnum.LOGOUT,
      sessionID: this.getSessionFromLocalStorage().session,
      user: this.getSessionFromLocalStorage().user,
    };
    this.webSocketService.sendIMessage(iMessageLogOut);
    this.webSocketService.getWebSocket().unsubscribe();
    this.removeSessionFromLocalStorage();
  }

}
