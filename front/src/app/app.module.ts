import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './components/app.component';
import { ReactiveFormsModule } from "@angular/forms";
import { PortalComponent } from './components/portal/portal.component';
import { NgParticlesModule } from "ng-particles";
import { ProfileComponent } from './components/profile/profile.component';
import { SecurityService } from './services/security/security.service';
import { WebsocketService } from './services/websocket/websocket.service';
import { AddWorkspaceComponent } from './components/profile/add-workspace/add-workspace.component';
import { AddChannelComponent } from './components/profile/add-channel/add-channel.component';
import { AddMemberComponent } from './components/profile/add-member/add-member.component';
import { ChannelComponent } from './components/profile/channel/channel.component';
import { WorkspaceMembersComponent } from './components/profile/workspace-members/workspace-members.component';
import { UpdateWorkspaceComponent } from './components/profile/update-workspace/update-workspace.component';

@NgModule({
  declarations: [
    AppComponent,
    PortalComponent,
    ProfileComponent,
    AddWorkspaceComponent,
    AddChannelComponent,
    AddMemberComponent,
    ChannelComponent,
    WorkspaceMembersComponent,
    UpdateWorkspaceComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    NgParticlesModule
  ],
  providers: [
    SecurityService,
    WebsocketService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
